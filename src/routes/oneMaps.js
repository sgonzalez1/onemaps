const express = require('express');
const router = express.Router();
const pool = require('../database');
const { isLoggedIn, isNotLoggedIn } = require('../lib/auth');

router.get('/mapa', isLoggedIn, async(req, res) => {
    const pozos = await pool.query('SELECT * FROM tbpozos');
    res.locals.gridData = await JSON.stringify(pozos);
    res.render('maps/onemaps');
});

router.get('/_mapa', async(req, res) => {
    const pozos = await pool.query('SELECT * FROM tbpozos');
    let gridData = await JSON.stringify(pozos);
    res.send(gridData);
});

router.get('/filtrar', isLoggedIn, async(req, res) => {
    res.render('Clasificador/oneFilter');
});
module.exports = router;