class mapUI {



  renderSelect(elementos) {
    const select = document.getElementById('selectMap');

    const _select = document.createElement('select');
    _select.setAttribute('id', 'selectMapa');
    debugger
    select.innerHTML = '';
    select.appendChild(_select);
    elementos.sort();
    elementos.forEach(element => {
      var opc = document.createElement("option");
      opc.setAttribute("value", element);
      var t = document.createTextNode(element);
      opc.appendChild(t);
      document.getElementById("selectMapa").appendChild(opc);
    });

  }

  renderRemove() {
    var elementX = document.getElementById('container-remove');
    if (elementX.innerHTML != "") {
      elementX.innerHTML = "";
      return;
    }
    var columnas =
        ``;
    arrayLayers.forEach(itemX => {
      columnas +=
        `<tr>
       <th>${itemX.name}</th>
       <td>${itemX.tipo}</td>
       <td><button onclick="Cmapa.eliminarLayerKriging('${itemX.name}')"><i class="fas fa-trash-alt"></i></button><button onclick="Cmapa.eliminarLayerKriging('${itemX.name}')"><i class="fas fa-wrench"></i></button></td>
     </tr>`
    })

    const table = //html
      ` 
       <table class="table">
       <thead>
         <tr>
           <th scope="col">Nombre</th>
           <th scope="col">Tipo</th>
           <th scope="col">Eliminar</th>
         </tr>
       </thead>
       <tbody>
       ${columnas}
       </tbody>
     </table>`;
    elementX.innerHTML = table;
  }


  renderBurbujas() {
    var elementX = document.getElementById('container-burbuja');
    if (elementX.innerHTML != "") {
      elementX.innerHTML = "";
      return;
    }
    var local = [];
    debugger
    var opcTabla = '',
      opcLongitud = '',
      opcLatitud = '',
      opcVariable = '';
    for (var y = 0; y < localStorage.length; y++) {
      // <option value="australia">Australia</option>
      opcTabla += '<option value="' + localStorage.key(y) + '">' + localStorage.key(y) + '</option>';
      let elementoLocal = localStorage.getItem(localStorage.key(y));
      local.push({
        nombre: localStorage.key(y),
        data: JSON.parse(elementoLocal)
      })
    }

    var cad = //html
      `<div class="container">
              <form>
                <div class="row">
                  <div class="col-25">
                   <label for="slcTabla">Tabla</label>
                  </div>
                  <div class="col-75">
                    <select onchange='slcEventChange(this.value)' id="slcTabla" name="Tabla">
                      <option value="-1">Seleccione...</option>
                      ${opcTabla}
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="slcLatitud">Latitud</label>
                  </div>
                  <div class="col-75">
                    <select id="slcLatitud" name="Latitud">
                      <option value="-1">Seleccione...</option>
                       ${opcLatitud}
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="slcLongitud">Longitud</label>
                  </div>
                  <div class="col-75">
                    <select id="slcLongitud" name="Longitud">
                      <option value="-1">Seleccione...</option>
                        ${opcLongitud}
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">    
                  </div>
                  <div class="col-75">
                    <input id="coorXYtoLatLong" type="checkbox" name="coorXYtoLatLong" value="Boat">
                    <label for="coorXYtoLatLong">Convertir a Lat-Long</label>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="slcGrafica">Grafica</label>
                  </div>
                  <div class="col-75">
                    <select id="slcGrafica" name="Grafica">
                      <option value="0">Seleccione...</option>
                      <option value="Burbuja">Burbujas</option>
                      <option value="Anotaciones">Anotaciones (Poligono)</option>
                      <option value="AnotacionesLinea">Anotaciones (Lineas)</option>
                      <option value="AnotacionesPuntos">Anotaciones (Puntos)</option>
                      <option value="Kriging">kriging</option>
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="slcVariable">Variable</label>
                  </div>
                  <div class="col-75">
                    <select id="slcVariable" name="Variable">
                      <option value="-1">Seleccione...</option>
                      ${opcVariable}
                    </select>
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="slcVariable">Color</label>
                  </div>
                  <div class="col-75">
                    <input id='color' type="color" value="red">
                    <input id='color' type="color" value="red">
                  </div>
                </div>
                <div class="row">
                  <div class="col-25">
                    <label for="slcTabla">Nombre</label>
                  </div>
                  <div class="col-75">
                    <input type="text" placeholder="Ingresar nombre" id="slcNombre" name="Nombre"> 
                  </div>
                </div>
                <div class="row">
                  <input class='btnsubmit' id='submitEvent' onclick="processForm(this)" type="button" value="Aplicar">
                </div>
              </form>
            </div>`;
    elementX.innerHTML = cad;
  }




}