'use strict';
class TableUI {
    renderTable(col, data, percent = 0.05) {
        const elemento = document.getElementById('app');
        elemento.innerHTML = '';

        var _col = //html
            `<div class="table-box"><table class="one-table" cellpadding="10"><tr>`;

        for (let i = 0; i < col.length; i++) {
            var elementoX = col[i];
            _col += `<th>${elementoX}</th>`;
        }
        _col += `</tr>`;

        var _data = '';
        var count = data.length * percent;
        for (let y = 0; y < count; y++) {
            var elementoY = Object.values(data[y]);
            _data += `<tr>`
            for (let z = 0; z < elementoY.length; z++) {
                _data += `<td>${elementoY[z]}</td>`;
            }
            _data += `</tr>`
        }
        _col += _data;
        _col += `</table></div>`;

        debugger;

        elemento.innerHTML = _col;
        return _col;
    }


    renderSelect(_element, array) {
        const elemento = document.getElementById(_element);

        var au = "<select style='background:#8d8787;' onchange='prueba(this.value)' name='deporte'> <option value='-1'>Seleccionar tabla</option>";
        var aux = "";
        for (let i = 0; i < array.length; i++) {
            aux = "<option value='" + i + "'>" + array[i] + "</option>"
            au += aux;
        }
        au += "</select>"
        debugger;
        elemento.className = 'multiselect';
        elemento.innerHTML = au;
    }

    renderSelectEspecial(_element, array) {
        const elemento = document.getElementById(_element);
        debugger
        var au = //html
            `<form>
                    <div class='multiselect'>
                        <div class='selectBox_'>
                            <button type='button' onclick='btnTipo("box${_element}")' class='btn btn-outline-dark btn-sm'>Tipo</button>
                            <button type='button' onclick='btnValor("box${_element}")' class='btn btn-outline-dark btn-sm'>Valor</button>
                            <button type='button' class='btn btn-outline-dark btn-sm'>Fecha</button>
                        </div>
                        <div class='selectBox' onclick='showCheckboxesEspecial("checkboxes${_element}")'>
                            <select>
                                <option>${_element.replace('itemsX', '').toUpperCase()}</option>
                            </select>
                            <div class='overSelect'></div> 
                        </div>
                        <div id='checkboxes${_element}'><div class='search-box'>
                        <input class='search-txt' type='text' name='' onkeyup='_buscar(this.value, \"cbx${_element.replace("itemsX", "")}\");' placeholder='Buscar...'>
                        <a class='search-btn' href='#'>
                            <i class='fas fa-bullhorn'></i>
                        </a>
                    </div>
                    <hr>
                    <div id='box${_element}'>`;
        for (let i = 0; i < array.length; i++) {
            var aux = //html
                `<label class='cbx${_element.replace('itemsX', '')}' for='cbx${_element.replace('itemsX', '')+i}'>
                     <input onchange='cbxClasificar("${_element.replace("itemsX", "")}", this);' type='checkbox' id='cbx${_element.replace('itemsX', '')+i}'/> 
                     <span class='checkmark'></span>
                        ${array[i]}
                </label>`;
            au += aux;
        }
        au += `</div> </div> </div> </form>`;
        debugger;
        elemento.innerHTML = au;
        showCheckboxesEspecial(`checkboxes${_element}`);
    }

    _renderSelectEspecial(_element, _name, array) {
        const elemento = document.getElementById(_element);
        debugger
        var au = "";
        var aux = "<br>";
        for (let i = 0; i < array.length; i++) {
            aux = "<label for='cbx" + _name + i + "'> <input class='cbx" + _name + "' type='checkbox' onclick='eventChecked(this)' id='cbx" + _name + i + "'/><span class='checkmark'></span>" + array[i] + "</label>";
            au += aux;
        }
        debugger;
        elemento.innerHTML = au;
    }
    _renderItemXCheckbox() {

    }
}