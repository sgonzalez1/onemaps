class OneMap {

    arrayLayers = new ol.Collection()

    arrayAux = [];
    constructor(elementosSource, nameCoordenadaX = 'Latitud', nameCoordenadaY = 'Longitud') {

        var features = new Array(elementosSource.length);

        for (let i = 0; i < elementosSource.length; i++) {
            let elemento = elementosSource[i];
            coordinates = ol.proj.fromLonLat([elemento.Longitud, elemento.Latitud]);

            features[i] = new ol.Feature({
                geometry: new ol.geom.Point(coordinates),
                name: elemento.Pozo + '*' + elemento.Campo + '*' + elemento.Ubicacion + '*' + elemento.CompaniaPerfo + '*' + elemento.FechaInicioProduccion
            });
        }
        this.array = features;
        this.nameCoordenadaX = nameCoordenadaX;
        this.nameCoordenadaY = nameCoordenadaY;
        this.arrayAux = elementosSource;
        //console.log(this.arrayAux)
    }

    set nameCoordenadaX_(val) {
        this.nameCoordenadaX = val;
    }
    set nameCoordenadaY_(val) {
        this.nameCoordenadaY = val;
    }

    setArray(elementosSource) {

        var features = [];
        this.array = [];

        for (let i = 0; i < elementosSource.length; i++) {
            let elemento = elementosSource[i];
            coordinates = ol.proj.fromLonLat([elemento[this.nameCoordenadaY], elemento[this.nameCoordenadaX]]);

            features.push(new ol.Feature({
                geometry: new ol.geom.Point(coordinates),
                name: elemento.Pozo + '*' + elemento.Campo + '*' + elemento.Ubicacion + '*' + elemento.CompaniaPerfo + '*' + elemento.FechaInicioProduccion
            }))
        }
        this.array = features;

        this.arrayAux = elementosSource;
        // console.log(this.arrayAux)
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////CONVERSOR DE COORDENADAs/////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    UTMtoLL(f, f1, j) {
        var d = 0.99960000000000004;
        var d1 = 6378137;
        var d2 = 0.0066943799999999998;

        var d4 = (1 - Math.sqrt(1 - d2)) / (1 + Math.sqrt(1 - d2));
        var d15 = f1 - 500000;
        var d16 = f;
        var d11 = ((j - 1) * 6 - 180) + 3;

        var d3 = d2 / (1 - d2);
        var d10 = d16 / d;
        var d12 = d10 / (d1 * (1 - d2 / 4 - (3 * d2 * d2) / 64 - (5 * Math.pow(d2, 3)) / 256));
        var d14 = d12 + ((3 * d4) / 2 - (27 * Math.pow(d4, 3)) / 32) * Math.sin(2 * d12) + ((21 * d4 * d4) / 16 - (55 * Math.pow(d4, 4)) / 32) * Math.sin(4 * d12) + ((151 * Math.pow(d4, 3)) / 96) * Math.sin(6 * d12);
        var d13 = d14 * 180 / Math.PI;
        var d5 = d1 / Math.sqrt(1 - d2 * Math.sin(d14) * Math.sin(d14));
        var d6 = Math.tan(d14) * Math.tan(d14);
        var d7 = d3 * Math.cos(d14) * Math.cos(d14);
        var d8 = (d1 * (1 - d2)) / Math.pow(1 - d2 * Math.sin(d14) * Math.sin(d14), 1.5);

        var d9 = d15 / (d5 * d);
        var d17 = d14 - ((d5 * Math.tan(d14)) / d8) * (((d9 * d9) / 2 - (((5 + 3 * d6 + 10 * d7) - 4 * d7 * d7 - 9 * d3) * Math.pow(d9, 4)) / 24) + (((61 + 90 * d6 + 298 * d7 + 45 * d6 * d6) - 252 * d3 - 3 * d7 * d7) * Math.pow(d9, 6)) / 720);
        d17 = d17 * 180 / Math.PI;
        var d18 = ((d9 - ((1 + 2 * d6 + d7) * Math.pow(d9, 3)) / 6) + (((((5 - 2 * d7) + 28 * d6) - 3 * d7 * d7) + 8 * d3 + 24 * d6 * d6) * Math.pow(d9, 5)) / 120) / Math.cos(d14);
        d18 = d11 + d18 * 180 / Math.PI;
        return [d18, d17];
    }


    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////CREACION DE BURBUJAS////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    setArrayBurbuja(val, variable, color = '#f1c40f') {
        var circle = [];
        let arrayVariable = [];
        val.forEach((elementX) => {
            arrayVariable.push(elementX[variable]);
        });
        Array.prototype.max = function () {
            return Math.max.apply(null, this);
        };
        var max = arrayVariable.max();
        var max_ = val.length;
        val.forEach((elementX, index) => {

            var total = elementX[variable] / max;

            var algo = index / max_;

            coordinates = ol.proj.fromLonLat(this.UTMtoLL(elementX[this.nameCoordenadaY], elementX[this.nameCoordenadaX], 15));

            circle.push(new ol.Feature(new ol.geom.Circle(coordinates, (600 * algo / total) * total)))
        });

        var circleSource = new ol.source.Vector({
            features: circle
        });
        //rgba(255,0,0,0.2)
        var circleLayer = new ol.layer.Vector({
            id: 'Circles',
            source: circleSource,
            style: function () {

                let hexToRgb = (hex) => {
                    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                    return result ? 'rgba(' + parseInt(result[1], 16) + ',' + parseInt(result[2], 16) + ',' + parseInt(result[3], 16) + ',' + 0.1 + ')' : null;
                }

                var size1 = document.size;
                var style1 = styleCache1[size1];
                style1 = new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        width: 3,
                        color: '#2c3e50'
                    }),
                    fill: new ol.style.Fill({
                        color: hexToRgb(color)
                    })
                })
                styleCache1[size1] = style1;
                return style1;
            }
        });
        return circleLayer;
    }



    ////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////KRIGING 1.0//////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    renderCuadrante(arrayData_, nombre) {
        var nombre = nombre;
        debugger;

        arrayData_.forEach((elementX) => {

            var arrayX = []
            var arrayY = []
            var vulues1_ = []

            var feature = null;

            // DEFINICION DE PARAMETROS START
            let params = {
                krigingModel: 'exponential',//El modelo también selecciona 'gaussian', 'spherical'
                krigingSigma2: 0.001,
                krigingAlpha: 100,
                canvasAlpha: 0.75,//Transparencia de la capa del lienzo
                colors: ["#006837", "#1a9850", "#66bd63", "#a6d96a", "#d9ef8b", "#ffffbf",
                    "#fee08b", "#fdae61", "#f46d43", "#d73027", "#a50026"],
            };

            let WFSVectorSource = new ol.source.Vector();

            let WFSVectorLayer = new ol.layer.Vector(
                {
                    source: WFSVectorSource,
                });
            map.addLayer(WFSVectorLayer);

            // DEFINICION DE PARAMETROS END

            //Agregue controles de selección y selección de cuadros, mantenga presionada la tecla Ctrl / ⌘, use el mouse para seleccionar puntos de muestra
            let select = new ol.interaction.Select();
            map.addInteraction(select);
            let dragBox = new ol.interaction.DragBox({
                condition: ol.events.condition.platformModifierKeyOnly
            });
            map.addInteraction(dragBox);
            //设置框选事件
            let selectedFeatures = select.getFeatures();
            dragBox.on('boxend', () => {
                let extent = dragBox.getGeometry().getExtent();
                WFSVectorSource.forEachFeatureIntersectingExtent(extent, (feature) => {
                    selectedFeatures.push(feature);
                });
                drawKriging(extent);
            });
            dragBox.on('boxstart', () => {
                selectedFeatures.clear();
            });


            //CREACION DE PUNTOS Y SUS VALORES START

            elementX.forEach((elementoY) => {

                arrayX.push(elementoY.coordenadasXY[1])
                arrayY.push(elementoY.coordenadasXY[0])
                vulues1_.push(elementoY.value);
                let feature = new ol.Feature({
                    geometry: new ol.geom.Point([elementoY.coordenadasXY[1],
                    elementoY.coordenadasXY[0]]),
                    value: elementoY.value
                });
                feature.setStyle(new ol.style.Style({
                    image: new ol.style.Circle({
                        radius: 6,
                        fill: new ol.style.Fill({ color: "#00F" })
                    })
                }));
                WFSVectorSource.addFeature(feature);

            })

            var elementoY = elementX[0];
            arrayX.push(elementoY.coordenadasXY[1])
            arrayY.push(elementoY.coordenadasXY[0])
            vulues1_.push(elementoY.value);
            let feature_ = new ol.Feature({
                geometry: new ol.geom.Point([elementoY.coordenadasXY[1],
                elementoY.coordenadasXY[0]]),
                value: elementoY.value
            });
            feature_.setStyle(new ol.style.Style({
                image: new ol.style.Circle({
                    radius: 6,
                    fill: new ol.style.Fill({ color: "#00F" })
                })
            }));
            WFSVectorSource.addFeature(feature_);



            //Establecer evento de selección de marco
            selectedFeatures = select.getFeatures();
            dragBox.on('boxend', () => {

                let extent = dragBox.getGeometry().getExtent();
                WFSVectorSource.forEachFeatureIntersectingExtent(extent, (feature) => {
                    selectedFeatures.push(feature);
                });
                drawKriging(extent);
            });
            dragBox.on('boxstart', () => {
                selectedFeatures.clear();
            });

            //DELIMITACION DE LOS MARGENES DEL CUADRADO
            var yMax = Math.max.apply(null, arrayY);
            var yMin = Math.min.apply(null, arrayY);
            var xMax = Math.max.apply(null, arrayX);
            var xMin = Math.min.apply(null, arrayX);

            var rectanguleichon = [
                yMin,
                xMax,
                yMax,
                xMin];

            //Dibujo del mapa de interpolación de kriging
            let canvasLayer = null;
            const drawKriging_ = (extent) => {

                let values = [], lngs = [], lats = [];

                selectedFeatures.forEach(feature => {

                    values.push(feature.values_.value);
                    lngs.push(feature.values_.geometry.flatCoordinates[0]);
                    lats.push(feature.values_.geometry.flatCoordinates[1]);
                });


                if (vulues1_.length > 3) {
                    let variogram = kriging.train(vulues1_, arrayY, arrayX,
                        params.krigingModel, params.krigingSigma2, params.krigingAlpha);

                    let polygons = [];
                    polygons.push([[extent[0], extent[1]], [extent[0], extent[3]],
                    [extent[2], extent[3]], [extent[2], extent[1]]]);
                    let grid = kriging.grid(polygons, variogram, (extent[2] - extent[0]) / 200);

                    let dragboxExtent = extent;
                    //Eliminar capa existente
                    if (canvasLayer !== null) {
                        map.removeLayer(canvasLayer);
                    }
                    //Crea una nueva capa
                    canvasLayer = new ol.layer.Image({
                        id: nombre,
                        source: new ol.source.ImageCanvas({
                            canvasFunction: (extent, resolution, pixelRatio, size, projection) => {
                                let canvas = document.createElement('canvas');
                                canvas.width = size[0];
                                canvas.height = size[1];
                                canvas.style.display = 'block';
                                //Establecer transparencia de lienzo
                                canvas.getContext('2d').globalAlpha = params.canvasAlpha;

                                //Renderizado con color en capas
                                kriging.plot(canvas, grid,
                                    [extent[0], extent[2]], [extent[1], extent[3]], params.colors);
                                return canvas;
                            },
                            projection: 'EPSG:4326'
                        })
                    })
                    //Agregar una capa al mapa
                    map.addLayer(canvasLayer);


                } else {
                    alert("Número insuficiente de muestras válidas, incapaz de interpolar");
                }
            }

            //Cargue la primera vez, renderice automáticamente el gráfico de diferencia una vez
            let extent = [rectanguleichon[0], rectanguleichon[1], rectanguleichon[2], rectanguleichon[3]];
            WFSVectorSource.forEachFeatureIntersectingExtent(extent, (feature) => {

                selectedFeatures.push(feature);
            });
            drawKriging_(extent);

        })
    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////SET KRIGING//////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    setKriging(arrayData, variable, id) {


        var nombre = id;
        var itemW = [];
        var itemX = [];
        var itemY = [];
        var cuadrante = [];
        var cuadrante2 = [];
        var nKriging = 500
        var valueM = []
        var valueMax = []
        var valueMin = []
        var nombre = nombre;

        //AREA DE CUADRANTE

        // FUNCION PARA ORDENAR EL ARRAY DE MENOR A MAYOR -COMIENZA-
        arrayData.forEach((elementX, index) => {

            if (elementX[this.nameCoordenadaX] != "" && elementX[this.nameCoordenadaY] != "" && elementX[variable] != "0")
                itemY.push({
                    'coordenadasXY': this.UTMtoLL(elementX[(this.nameCoordenadaY)], elementX[(this.nameCoordenadaX)], 15),
                    'value': elementX[variable]
                })

            itemY.sort(function (a, b) {
                if (a.coordenadasXY > b.coordenadasXY) {
                    return 1;
                }
                if (a.coordenadasXY < b.coordenadasXY) {
                    return -1;
                }
                return 0;
            })

        })
        // FUNCION PARA ORDENAR EL ARRAY DE MENOR A MAYOR -END-

        //ENCONTRAR LOS VALORES MAX Y MIN -START-
        itemY.forEach((elementX, index) => {
            valueM.push(elementX.value)
        })

        valueMax = Math.max.apply(null, valueM);
        valueMin = Math.min.apply(null, valueM);


        arrayData.forEach((elementX, index) => {
            if (elementX[variable] == valueMax || elementX[variable] == valueMin) {
                itemW.push({
                    'coordenadasXY': this.UTMtoLL(elementX[(this.nameCoordenadaY)], elementX[(this.nameCoordenadaX)], 15),
                    'value': elementX[variable]
                })
            }
        })
        //ENCONTRAR LOS VALORES MAX Y MIN -END-

        // FUNCION PARA CREAR LOS CUADRANTES -START-
        var hash = {};
        itemY.forEach((elementX, index) => {

            if (itemX.length < nKriging) {
                itemX.push({
                    'coordenadasXY': elementX.coordenadasXY,
                    'value': elementX.value
                })
            }

            if (itemX.length == nKriging) {

                // cuadrante[0]=itemX;

                cuadrante.push(itemX)

                itemX = []

            }

        })

        if (itemX.length > 0)

            //cuadrante[0]=itemX;
            itemX = itemX.filter(function (current) {
                var exists = !hash[current.coordenadasXY] || false;
                hash[current.coordenadasXY] = true;
                return exists;
            });
        cuadrante.push(itemX)
        // FUNCION PARA CREAR LOS CUADRANTES -END-

        // console.log(cuadrante)
        // console.log(itemY)

        // console.log(valueMax)
        // console.log(valueMin)

        cuadrante2.push(itemY)

        //Solo se usa  el primero
        this.renderCuadrante(cuadrante, nombre)
        debugger;


    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////ANOTACIONES//////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////ANOTACIONES (Poligono)/////////////////////////////////////

    setArrayPolygon(arrData, color, id) {
        var nombre = id;

        const coorSetArrayLine = [];

        // FUNCION PARA ORDENAR EL ARRAY DE MENOR A MAYOR -COMIENZA-
        arrData.forEach(elementX =>
            coorSetArrayLine.push(ol.proj.fromLonLat(this.UTMtoLL(elementX[this.nameCoordenadaY],
                elementX[this.nameCoordenadaX], 15))));

        coorSetArrayLine.sort()

        // FUNCION PARA ORDENAR EL ARRAY DE MENOR A MAYOR -END-

        var lineFeature = new ol.Feature(new ol.geom.LineString(coorSetArrayLine));

        let retorno = new ol.layer.Vector({
            id: nombre,
            source: new ol.source.Vector({
                features: [lineFeature]
            }),
            style: function () {

                let hexToRgb = (hex) => {
                    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                    //rgba(255,0,0,0.2)
                    let vari = 'rgba(' + result[1] + ',' + result[2] + ',' + result[3] + ')';

                    return result ? 'rgba(' + parseInt(result[1], 16) + ',' + parseInt(result[2], 16) + ',' + parseInt(result[3], 16) + ',' + 0.2 + ')' : null;
                }

                var size1 = document.size;
                var style1 = styleCache2[size1];
                style1 = new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        width: 3,
                        color: hexToRgb(color)
                    }),
                    fill: new ol.style.Fill({
                        color: '#2c3e50'
                    })
                })
                styleCache2[size1] = style1;
                return style1;
            }
        })
        return retorno;

    }

    /////////////////////////////////ANOTACIONES (Linea)/////////////////////////////////////

    setArrayLine(arrData, color, id) {
        var nombre = id;

        const coorSetArrayLine = [];
        arrData.forEach(elementX => coorSetArrayLine.push(ol.proj.fromLonLat(this.UTMtoLL(elementX[this.nameCoordenadaY], elementX[this.nameCoordenadaX], 15))));
        var lineFeature = new ol.Feature(new ol.geom.LineString(coorSetArrayLine));

        let retorno = new ol.layer.Vector({
            id: nombre,
            source: new ol.source.Vector({
                features: [lineFeature]
            }),
            style: function () {

                let hexToRgb = (hex) => {
                    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
                    //rgba(255,0,0,0.2)
                    let vari = 'rgba(' + result[1] + ',' + result[2] + ',' + result[3] + ')';

                    return result ? 'rgba(' + parseInt(result[1], 16) + ',' + parseInt(result[2], 16) + ',' + parseInt(result[3], 16) + ',' + 0.2 + ')' : null;
                }

                var size1 = document.size;
                var style1 = styleCache2[size1];
                style1 = new ol.style.Style({
                    stroke: new ol.style.Stroke({
                        width: 3,
                        color: hexToRgb(color)
                    }),
                    fill: new ol.style.Fill({
                        color: '#2c3e50'
                    })
                })
                styleCache2[size1] = style1;
                return style1;
            }
        })
        return retorno;

    }

    /////////////////////////////////ANOTACIONES (Puntos)/////////////////////////////////////

    setArrayPuntos(arrData, color, id) {
        var nombre = id;

        const coorSetArrayLine = [];
        arrData.forEach(elementX => coorSetArrayLine.push(ol.proj.fromLonLat(this.UTMtoLL(elementX[this.nameCoordenadaY], elementX[this.nameCoordenadaX], 15))));
        var puntoFeature = new ol.Feature(new ol.geom.MultiPoint(coorSetArrayLine));

        let retorno = new ol.layer.Vector({
            id: nombre,
            source: new ol.source.Vector({
                features: [puntoFeature]
            })
        })
        return retorno;

    }

    ////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////ELIMINACIONES/////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////



    eliminarLayerKriging(id) {
        map.getLayers().getArray().filter(layer => layer.get('id') === id)
            .forEach(layer => map.removeLayer(layer));

  
    }

    showLayers(){
        console.log(map.getLayers().getArray())
    }




    ////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////CLUSTER////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////

    crearClusterSource(distancia) {

        var source_ = new ol.source.Vector({
            features: this.array
        });
        var miClusterSource = new ol.source.Cluster({
            distance: distancia,
            source: source_
        });

        return miClusterSource
    };

    xFunction(Element) {
        return this.arrayAux.filter(x => x.Campo == Element);
    }

    agregarCluster(clusterSourceK, colorK) {
        var cluster_ = new ol.layer.Vector({
            source: clusterSourceK,
            style: function (feature) {
                var size = feature.get('features').length;
                var style = styleCache[size];
                if (true) {
                    style = new ol.style.Style({
                        image: new ol.style.Circle({
                            radius: 5,
                            stroke: new ol.style.Stroke({
                                color: colorK
                            }),
                            fill: new ol.style.Fill({
                                color: colorK
                            })
                        }),
                        text: new ol.style.Text({
                            font: '18px Calibri,sans-serif',
                            text: feature.get('features')[0].get('name').split('*')[0],
                            fill: new ol.style.Fill({
                                color: '#34495E'
                            }),
                        })
                    });
                    styleCache[size] = style;
                }
                return style;
            }
        })
        return cluster_;
    };

    OpenPopup(evt) {
        var featureOpenPopup = map.forEachFeatureAtPixel(evt.pixel,
            function (featureOpenPopup, layer) {
                if (featureOpenPopup) {
                    var coord = map.getCoordinateFromPixel(evt.pixel);
                    if (typeof featureOpenPopup.get('features') === 'undefined') {
                        popup_content.innerHTML = '<h5><b>' + featureOpenPopup.get('name') + '</b></h5><i>this is an <b>unclustered</b> feature</i>';
                    } else {
                        var cfeatures = featureOpenPopup.get('features');
                        if (cfeatures.length > 1) {
                            popup_content.innerHTML = '<h5><strong>all "Sub-Features"</strong></h5>';

                            var cad = '<table class="table table-bordered"><thead><tr><th scope="col">#</th><th scope="col">Pozo</th><th scope="col">Campo</th> <th scope="col">Ubicación</th><th scope="col">Compañia Perfo.</th></tr></thead>  <tbody>';
                            for (let i = 0; i < cfeatures.length; i++)
                                cad += '<tr><th scope="row">' + i + ' </th><td>' + cfeatures[i].get('name').split('*')[0] + '</td><td>' + cfeatures[i].get('name').split('*')[1] + '</td><td>' + cfeatures[i].get('name').split('*')[2] + '</td><td>' + cfeatures[i].get('name').split('*')[3] + '</td></tr>';


                            cad += `</tbody></table>`
                            popup_content.innerHTML = cad;
                        }
                        if (cfeatures.length == 1) {
                            // popup_content.innerHTML = '<h5><strong>' + cfeatures[0].get('name').split('*')[0] + '</strong></h5><i>this is a single, but <b>clustered</b> feature</i>';

                            popup_content.innerHTML = //html
                                `
                  <table class="table table-striped table-black table-bordered">
                          <tbody>
                            <tr>
                            <td>Pozo</td>
                              <td>${cfeatures[0].get('name').split('*')[0]}</td>
                            
                            </tr>
                            <tr>
                            <td>Fecha</td>
                              <td>${cfeatures[0].get('name').split('*')[1]}</td>
                              
                            </tr>
                            <tr>
                            <td>Comentario</td>
                            <td>${cfeatures[0].get('name').split('*')[2]}</td>
                          
                          </tr>
                          <tr>
                          <td>Uwi</td>
                          <td>${cfeatures[0].get('name').split('*')[3]}</td>
    
                        </tr>
                            
                          </tbody>
                </table>
    
                  
                 `
                        }
                    }
                    olpopup.setPosition(coord);
                } else {
                    olpopup.setPosition(undefined);
                }
            });
    };
};


//Crear un array con objetos, nombre, id
// Agregar un menu de configuraciones para cambiar las propiedades del layer que se desee.
// Al momento de presionar el boton "aplicar" se debera de asignar un nombre al la6yer que se este creando para que se guarde en un array local.
// Una vez creado el layer se podra cambiar sus propiedades en el nuevo menu llamado "configuraciones"
// En el menu de configuraciones se le opodra cambiar el color y se podra eliminar el layer elegido en cualer momento, pero no se podra recuperar