this.importScripts('/controllerUI/xlsx.full.min.js');
this.addEventListener('message', (e) => {
    var data = new Uint8Array(e.data);
    var arr = new Array();
    for (var i = 0; i != data.length; ++i) arr[i] = String.fromCharCode(data[i]);
    var bstr = arr.join(""); /* Call XLSX */
    var workbook = XLSX.read(bstr, {
        type: "binary"
    });
    this.postMessage(workbook);
    // this.postMessage(workbook.SheetNames);
});