'use strict';

function modalEventAplicar(e) {
    debugger
    if (e != "")
        if (_Tabla)
            localStorage.setItem(e, JSON.stringify(_Tabla.getData_));
        else Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Tabla no creada!'
        });
    else Swal.fire({
        type: 'error',
        title: 'Oops...',
        text: 'Agregar identificador a la tabla!'
    });
    return

}

function btnTipo(e) {
    debugger;
    let cad = e.replace('boxitemsX', '');
    var _aux = _Tabla.getDataFilter(cad);
    instancia.renderSelectEspecial(`itemsX${cad}`, _aux);
}

function btnValor(e) {
    let cad = e.replace('boxitemsX', '');
    document.getElementById(e).innerHTML = //html
        `

        <div class="container" style='font-size:12px'>
        <div class="card">
            <div class="card-header">
                Agregar Valores
            </div>
            <div class="card-body">
                <div class="row">
                    <table class="table table-sm">
                        <thead>
                            <tr>
                                 <th scope="col">Operador</th>
                                 <th scope="col">Valor</th>
                                 <th scope="col">Opc.</th>
                            </tr>
                        </thead>
                    </table>
                </div>


                <div id='contendorValor' data-name='${cad}' class="">

                    <div style="paddig:1px;" id="itemRow_0" class="item row">
                        <select class="col-md-5" name="" id="">
                            <option value="0"><</option>
                             <option value="1">></option>
                            <option value="2">!=</option>
                            <option value="3">>=</option>
                            <option value="4"> <=</option> 
                            </select>
                             <input class="col-md-5" type="text" name="">
                        <button type="button" onclick="addElement(this);" class="btn btn-link btn-sm col-md-2">
                                        <i id="btnVlor0" class="fas fa-plus-circle"></i>
                                    </button>
                    </div>
                </div>

            </div>
        </div>
    </div>
    `;
}

var _indexVlor = 1;

function addElement(e) {


    if (e.parentElement.children[1].value == '') {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Agrega un valor!'
        });
        return
    }

    var miDiv = document.createElement('div');
    var aux = document.getElementById('btnVlor' + (_indexVlor - 1));
    var _elemento = document.getElementById('contendorValor');

    var dato = e.parentElement.children[1].value;
    var operador_ = e.parentElement.children[0].value;
    var parentName = e.parentElement.parentElement.dataset.name;
    var Id = e.parentElement.id.split('_')[1];
    debugger

    if (e.children[0].className.includes('fa-minus-circle')) {
        debugger;
        _elemento.removeChild(e.parentElement);
        _Tabla.removeElementJson(parentName, Id, 'Valor');
    } else {

        _Tabla.addElementJson(parentName, { 'Id': Id, 'Operador': operador_, 'Value': dato }, 'Valor');

        debugger
        miDiv.id = `itemRow_${_indexVlor}`;
        miDiv.className = 'item row';
        miDiv.innerHTML = //html
            `<select id='sltVlor${_indexVlor}' class="col-md-5" name="" id="">
            <option value="0"> <</option> 
            <option value="1">> </option>
            <option value="2">!=</option>
            <option value="3">>=</option>
            <option value="4"> <=</option> 
        </select>
        <input id='iptVlor${_indexVlor}' class="col-md-5" type="text" name="">
        <button type="button" onclick="addElement(this);" class="btn btn-link btn-sm col-md-2">
                    <i id='btnVlor${_indexVlor}' class="fas fa-plus-circle"></i>
        </button>`;



        aux.className = 'fas fa-minus-circle';
        aux.style.color = 'red';
        _elemento.appendChild(miDiv);
        _indexVlor++;
        debugger;
    }
}

function cbxClasificar(objectName, value_) {
    let _nodoName = objectName.replace("itemsX", "");
    let _value = value_.labels[0].innerText;

    if (value_.checked) _Tabla.addElementJson(_nodoName, _value, 'Tipo');
    else _Tabla.removeElementJson(_nodoName, _value, 'Tipo');

}

function btnEventClasificar() {
    debugger;
    let _data = _Tabla.getDataClasificar();
    let dataOriginal = _Tabla.getData_;
    let returnData = undefined;
    _data.forEach(element => {
        if (element.nodo.Tipo.length > 0) {
            debugger;
            returnData = _Tabla.getGroupBy(element.nodo.Tipo, element.nodo.Nombre);
            _Tabla.setData_ = returnData;
        } else if (element.nodo.Valor.length > 0) {
            debugger
            returnData = _Tabla.getGroupByValor(element.nodo.Valor, element.nodo.Nombre);
            _Tabla.setData_ = returnData;
        }
    });
    let dataOriginalClasificada = _Tabla.getData_;
    debugger
    var o = Object.keys(dataOriginalClasificada[0]);
    instancia.renderTable(o, dataOriginalClasificada, 1);
    _Tabla.setData_ = dataOriginal;
    _Tabla.imprimeJson();
}